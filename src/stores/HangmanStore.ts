import { EventEmitter } from 'events';
import ActionTypes from '../constants/constants';
import Dispatcher from './dispatcher';

 const CHANGE = 'CHANGE';
let theWord:string = '';
let guessedLetters: Set<string> = new Set();
let numberOfGuesses: number = 0;
class HangmanStore extends EventEmitter {
 
    constructor() {
        super();
        
        // Registers action handler with the Dispatcher.
        Dispatcher.register(this._registerToActions.bind(this));
    }
 
    // Switches over the action's type when an action is dispatched.
    _registerToActions(action:any) {
        switch(action.actionType) {
            case ActionTypes.GET_WORD:
                this._setWord(action.payload);
                break;
                case ActionTypes.GUESS_WORD:
                this._guessWord(action.payload);
                break;
        }
    }
 
     _guessWord(word:string){
         word.split('').map(letter=>{guessedLetters.add(letter.toLowerCase());});
         numberOfGuesses++;
         this.emit(CHANGE);
     }
    _setWord(word:string) {
        
        theWord= word;
        guessedLetters = new Set();
        
        numberOfGuesses= 0;
        this.emit(CHANGE);
    }
 
    // Returns the current word.
    getTheWord() {
        return theWord;
    }
    getNumberOfGuesses() {return numberOfGuesses;}
    getGuessedLetters(){

        return guessedLetters;
    };
 
    // Hooks a React component's callback to the CHANGED event.
    addChangeListener(callback:any) {
        this.on(CHANGE, callback);
    }
 
    // Removes the listener from the CHANGED event.
    removeChangeListener(callback:any) {
        this.removeListener(CHANGE, callback);
    }
}
 
export default new HangmanStore();