import ActionTypes from '../constants/constants';
import Dispatcher from '../stores/dispatcher';

 
 class HangmanActions {
 
     getWord(json:any) {
        // Note: This is usually a good place to do API calls.
        Dispatcher.dispatch({
            actionType: ActionTypes.GET_WORD,
            payload: json 
        });
    }
    guessWord(json:any) {
        // Note: This is usually a good place to do API calls.
        Dispatcher.dispatch({
            actionType: ActionTypes.GUESS_WORD,
            payload: json 
        });
    }
    
}
 
export default new HangmanActions();