import * as React from 'react';


export enum letterState {
   
    INCORRECT,
    CORRECT

}
interface ILetterProps extends React.Props<Letter> {
    status: letterState,
    letter: string
}


export class Letter extends React.Component<ILetterProps, {}> {
    constructor(props:ILetterProps) {
        super(props);
    }
 
   render() {
    return (
      <aside>
        {
            (this.props.status === letterState.CORRECT && this.props.letter) || '_'
        }
      </aside>
    );
  }
}

export default Letter;