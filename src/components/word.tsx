import * as React from 'react';
import * as lodash from 'lodash';
import HangmanStore from '../stores/HangmanStore';
import {Letter, letterState } from './letter';

import "./word.css";
interface IWordProps extends React.Props<Word> {
    
}

interface IWordState {
    guessedLetters: Set<string>,
    word:string
  
  

}
class Word extends React.Component<IWordProps, IWordState> {
    constructor(props:IWordProps) {
        super(props);
        this.state = {
            guessedLetters: HangmanStore.getGuessedLetters(),
            word: HangmanStore.getTheWord()
            
            
        };
        this._onChange = this._onChange.bind(this);
    }
 
     _onChange() {
        this.setState({ word: HangmanStore.getTheWord(),guessedLetters:HangmanStore.getGuessedLetters() });
    }
 
    componentWillMount() {
        HangmanStore.addChangeListener(this._onChange);
    }
    
    componentWillUnmount() {
        HangmanStore.removeChangeListener(this._onChange);
    }
    hasBeenGuessedCorrectly = (letter:string)=> {
        return lodash.includes(this.state.word.toLowerCase(),letter.toLowerCase())&& lodash.includes(Array.from(this.state.guessedLetters),letter.toLowerCase());

    }
   render() {
       const letters: React.ReactNode[] = [];
       this.state.word.split('').map(cLetter => {
        letters.push(React.createElement(Letter,{ letter:cLetter, status: this.hasBeenGuessedCorrectly(cLetter) ? letterState.CORRECT : letterState.INCORRECT  }));
      });
    return (
      <div className="WordContainer">
       {letters}
      
      </div>
    );
  }
}

export default Word;