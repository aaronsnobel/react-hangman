import * as React from 'react';
import HangmanStore from '../stores/HangmanStore';
import * as lodash from 'lodash';
import Word from './word';
import Noose from './noose';

interface IHangmanProps extends React.Props<Hangman> {
    
}

interface IHangmanState {
    guessed: Set<string>,
    numberWrong: number,
    word:string
}

class Hangman extends React.Component<IHangmanProps, IHangmanState> {
    
    constructor(props:IHangmanProps) {
        super(props);
       
       
        this.state = {
            guessed:HangmanStore.getGuessedLetters(),
            numberWrong: HangmanStore.getNumberOfGuesses(),
            word: HangmanStore.getTheWord()
           
            
        };
        this._onChange = this._onChange.bind(this);
    }
 
     _onChange() {
        this.setState({ word: HangmanStore.getTheWord(), guessed: HangmanStore.getGuessedLetters() });
    }
 
    componentWillMount() {
        HangmanStore.addChangeListener(this._onChange);
    }
 
    componentWillUnmount() {
        HangmanStore.removeChangeListener(this._onChange);
    }
    numberWrong = () => {
      return  lodash.difference(Array.from(this.state.guessed),this.state.word.split('')).length;

    }
    guessedTheWord = () => {
        let numRight = 0;
        this.state.word.split('').every((value:string):boolean=>{
                if(Array.from(this.state.guessed).indexOf(value) > -1){
                    numRight++;
                }
                return true;

        });
        return numRight === this.state.word.split('').length;
    }
  
   render() {
    return (
      <div className="Hangman">
        <header className="App-header">
          {this.state.word}
        </header>
        <Word />
        { this.guessedTheWord() && Array.from(this.state.guessed).length>0 &&
        <div> YOU WIN! </div>
        }
        {   (!this.guessedTheWord()&& this.numberWrong()>6 && 
                <div> restart! </div>)
            ||!this.guessedTheWord()&& <Noose numberWrong={this.numberWrong()}/>
        }
        
        
        
      </div>
    );
  }
}

export default Hangman;
