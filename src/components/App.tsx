import * as React from 'react';
import * as rword from 'rword';

import HangmanActions from '../actions/HangmanActions';
import Hangman from './Hangman'

import './App.css';

class App extends React.Component {
  changeWord = () =>{
    HangmanActions.getWord(rword.generate());
    rword.shuffle();

  }
  render() {
    return (
      <div className="App">
        <Hangman/>
        <button onClick={this.changeWord}>Change Word</button>
      </div>
    );
  }
}

export default App;
