import * as React from 'react';
import HangmanActions from '../actions/HangmanActions';

interface INooseProps extends React.Props<Noose> {
    numberWrong: number
}

class Noose extends React.Component<INooseProps, {}> {
    textInput: React.RefObject<HTMLInputElement>;
    constructor(props:INooseProps) {
        super(props);
        this.textInput = React.createRef();
    }
 
    makeGuess = () => {
       
         HangmanActions.guessWord((this.textInput.current as HTMLInputElement).value);
         (this.textInput.current as HTMLInputElement).value = '';
        
        
    }
 
   render() {
    
    return (<div>
      <img src={"imgs/" + this.props.numberWrong+".png"} />  
      <input type="text" ref={this.textInput}  />
      <button onClick={this.makeGuess}>Guess</button>
      </div>
    );
  }
}

export default Noose;
